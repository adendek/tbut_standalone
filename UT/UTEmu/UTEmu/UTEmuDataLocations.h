/*
 * UTEmuDataLocations.h
 *
 *  Created on: Oct 11, 2014
 *      Author: ADendek
 */

#pragma once
#pragma GCC diagnostic ignored "-Wunused-variable"

#include <string>

namespace UTEmu {
namespace DataLocations {
// should be extended
static const std::string& RawTES = "Tb/UTEmu/Raw";
static const std::string& HeaderTES = "Tb/UTEmu/Header";
static const std::string& PedestalTES = "Tb/UTEmu/Pedestal";
static const std::string& CMSTES = "Tb/UTEmu/CMS";
static const std::string& ZS_TES = "Tb/UTEmu/ZS";
static const std::string& Clusters_TES = "Tb/UTEmu/Clusters";
static const std::string& KeplerCluster_TES = "Tb/UTEmu/KeplerClusters";

static const std::string& MaskLocation =
    "$UTEMUSTANDALONEROOT//options/UT/ChannelMask_NO.dat";
static const std::string& PedestalLocation =
    "$UTEMUSTANDALONEROOT/options/UT/Pedestal.dat";
static const std::string& NoiseTreshold = "$UTEMUSTANDALONEROOT/options/UT/Noise.dat";
}
}
