/*
 * UTEmuMamdaDataReader.cpp
 *
 *  Created on: Jun 24, 2015
 *      Author: ADendek
 */

#include "UTEmuTxtDataReader.h"
#include <iostream>
#include <iostream>
#include <string>
#include <vector>
#include <limits.h>
#include <cmath>
#include <bitset>


using namespace UTEmu;
using namespace std;

TxtDataReader::TxtDataReader(std::string& p_fileName,
                                 IFileValidator& p_fileValidator)
    :  m_fileValidator(p_fileValidator),
       m_fileName(p_fileName)
 {}

void TxtDataReader::checkInput() {
  if (!m_fileValidator.validateFile()) {
    std::string errorMsg = "file validation error";
    throw IDataReader::InputFileError(errorMsg);
  }
  inputFile = fopen(m_fileName.c_str(), "rb");
  if (inputFile == NULL) {
    std::string errorMsg = "cannot open input file";
    throw IDataReader::InputFileError(errorMsg);
  }
}


RawData<>* TxtDataReader::getEventData() {
  if (feof(inputFile)) {
    string l_errorMsg = "No more event!";
    throw IDataReader::NoMoreEvents(l_errorMsg);
  }
  const int frameSize = 128;
  const int adc_length_in_bits = 6;

  const int bites_per_byte = 8;
  const int frame_len_in_bytes = (adc_length_in_bits*frameSize)/bites_per_byte;

   
  unsigned char bytes_array[frame_len_in_bytes];
  fread(bytes_array,1,frame_len_in_bytes, inputFile );

  auto signalVector = getDataFromBinFile(bytes_array, frame_len_in_bytes);
  RawData<>* outputData = new RawData<>();

  for (const auto& l_adc : signalVector) outputData->setSignal(l_adc);
  // set dummy value for each of the header
  outputData->setHeader0(1);
  outputData->setHeader1(1);
  outputData->setHeader2(1);
  outputData->setHeader3(1);
  outputData->setHeader3P1(1);
  outputData->setHeader3P2(1);
  outputData->setTime(1);
  outputData->setTDC(1);


  return outputData;
}


std::vector<int> TxtDataReader::getDataFromBinFile(unsigned char* array, int size)
{
    const int adc_per_frame = 128;
    std::vector<int> adcFromFile; 
    for(auto channel_it = 0 ; channel_it<adc_per_frame; channel_it++)
    {
        adcFromFile.push_back(get6bitNumber(array, size));
    }
    /*cout << "event adc-> ";
    for(const auto& adc :adcFromFile )
        cout << adc <<"-> ";
    cout<<endl;*/
    return adcFromFile;
}


int TxtDataReader::get_rs_mask(int shift)
{
    switch (shift)
    {
    case 0:
        return 0x00;
    case 1:
        return 0x01;
    case 2:
        return 0x03;
    case 3:
        return 0x07;
    case 4:
        return 0x0F;
    case 5:
        return 0x1F;
    case 6:
        return 0x3F;
    case 7:
        return 0x7F;
    default:
        throw "get_rs_mask -> Error, shift argument outside legal range 0-7";
    }
}

void TxtDataReader::shift_right(unsigned char* buf, int msg_len, int shift)
{
    unsigned char tmp = 0x00, tmp2 = 0x00;
    for (int bajt_it = 0; bajt_it <= msg_len; bajt_it++)
    {
        if (bajt_it == 0)
        {
            tmp = buf[bajt_it];
            buf[bajt_it] >>= shift;
        }
        else
        {
            tmp2 = buf[bajt_it];
            buf[bajt_it] >>= shift;
            buf[bajt_it] |= ((tmp & get_rs_mask(shift)) << (8 - shift));

            if (bajt_it != msg_len)
                tmp = tmp2;
        }
    }
}

int TxtDataReader::getNBits(unsigned char* array,int size,  int nbits)
{
    return array[size-1] & ((int)pow(2,nbits) - 1);
}

int TxtDataReader::get6bitNumber(unsigned char*  container,int size)
{

    auto sign = getNBits(container, size, 1);
    shift_right(container, size, 1);

    int n_bit = 5;
    auto adc = getNBits(container, size, n_bit);
    shift_right(container, size, n_bit);

    return (sign>0)? (-1)*adc: adc;

}




