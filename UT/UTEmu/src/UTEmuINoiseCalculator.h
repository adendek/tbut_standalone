/*
 * UTEmuINoiseCalculator.h
 *
 *  Created on: Jan 3, 2015
 *      Author: ADendek
 */

#pragma once

#include "UTEmu/UTEmuRawData.h"

namespace UTEmu {

class INoiseCalculator {
 public:
  virtual ~INoiseCalculator(){};
  virtual void updateNoise(RawData<double>* p_inputData) = 0;
  virtual void saveNoiseToFile(const std::string& p_filaname) = 0;
};
}
