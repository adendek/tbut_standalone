#pragma once

#include "GaudiAlg/GaudiAlgorithm.h"

// Local
#include "UTEmu/UTEmuRawData.h"
#include "UTEmuAlbavaFileValidator.h"
#include "UTEmuRawDataFactory.h"

#include <string>

namespace UTEmu {

class RawDataReaderAlgorithm : public GaudiAlgorithm {
 public:
  RawDataReaderAlgorithm(const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;

 private:
  void termiateApp();

  bool m_isStandalone;
  bool m_isAType;
  unsigned int m_eventNumber;
  unsigned int m_skipEventNumber;

  std::string m_inputDataOption;
  std::string m_alibavaInputData;
  std::string m_outputLocation;
  int m_sensorNumber;

  double m_mean;
  double m_sigma;

  UTEmu::AlbavaFileValidator m_fileValidator;
  UTEmu::RawDataFactory::DataReaderPtr m_rawDataReader;
  UTEmu::RawDataFactory m_inputDataFactory;
};
}
