/*
 * UTEmuPedestalSubtractor.h
 *
 *  Created on: Oct 14, 2014
 *      Author: ADendek
 */

#pragma once

#include "UTEmuIProcessingEngine.h"
#include "UTEmuPedestal.h"
#include "UTEmuIChannelMaskProvider.h"
#include "UTEmu/UTEmuRawData.h"

namespace UTEmu {

class PedestalSubtractor : public IProcessingEngine<> {
 public:
  PedestalSubtractor(Pedestal& p_pedestalSum,
                     IChannelMaskProvider& p_masksProvider);
  void processEvent(RawData<>* p_data, RawData<>** p_output) override;

 private:
  Pedestal& m_pedestal;
  IChannelMaskProvider& m_masksProvider;
};
}
