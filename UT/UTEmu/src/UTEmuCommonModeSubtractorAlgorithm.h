/*
 * UTEmuCommonModeSubtractorAlgorithm.h
 *
 *  Created on: Nov 24, 2014
 *      Author: ADendek
 */

#pragma once

#include "GaudiAlg/GaudiAlgorithm.h"

#include "UTEmu/UTEmuRawData.h"
#include "UTEmuCommonModeSubtractorFactory.h"
#include "UTEmuChannelMaskProvider.h"
#include "UTEmuChannelMaskFileValidator.h"
#include "UTEmuNoiseCalculatorfactory.h"
#include "UTEmu/UTEmuNoise.h"
#include <memory.h>
namespace UTEmu {

class CommonModeSubtractorAlgorithm : public GaudiAlgorithm {
  typedef std::shared_ptr<ICommonModeSubtractor> CMSubtractorPtr;

 public:
  CommonModeSubtractorAlgorithm(const std::string& name,
                                ISvcLocator* pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute() override;
  virtual StatusCode finalize() override;

 private:
  StatusCode initializeBase();
  StatusCode buildSubtractorEngine();
  StatusCode buildNoiseCalculator();
  StatusCode retriveMasksFromFile();

  StatusCode getData();
  void processEvent();
  StatusCode skippTreaningEvent();

  StatusCode saveNoiseToFile();

  RawDataContainer<>* m_dataContainer;
  RawData<>* m_data;
  RawDataContainer<double>* m_outputContainer;

  std::string m_inputDataLocation;
  std::string m_outputDataLocation;
  std::string m_channelMaskInputLocation;
  std::string m_CMSOption;
  std::string m_noiseCalculatorType;
  std::string m_noiseOutputLocation;

  int m_event;
  int m_skipEvent;

  ChannelMaskFileValidator m_channelMaskFileValidator;
  ChannelMaskProvider m_channelMaskProvider;
  CommonModeSubtractorFactory m_SubtractorFactory;
  CMSubtractorPtr m_CMSEnginePtr;
  NoiseCalculatorFactory m_noiseCalculatorFactory;
  NoiseCalculatorFactory::NoiseCalcualtorPtr m_noiseCalculatorPtr;
};
}
