/*
 * UTEmuNoiseCalculatorFake.h
 *
 *  Created on: Jan 3, 2015
 *      Author: ADendek
 */

#pragma once

#include "UTEmuINoiseCalculator.h"

namespace UTEmu {

class NoiseCalculatorFake : public INoiseCalculator {
 public:
  NoiseCalculatorFake();
  void updateNoise(RawData<double>* p_inputData) override;
  void saveNoiseToFile(const std::string& p_filaname) override;
};

} /* namespace UTEmu */
