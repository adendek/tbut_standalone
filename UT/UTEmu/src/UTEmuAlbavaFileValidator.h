/*
 * UTEmuAlbavaFileValidator.h
 *
 *  Created on: Oct 5, 2014
 *      Author: ADendek
 */

#pragma once
#include "UTEmuIFileValidator.h"
#include <filesystem>
#include <string>

namespace UTEmu {
class AlbavaFileValidator : public IFileValidator {
 public:
  AlbavaFileValidator(std::string& p_filename);
  bool validateFile() override;

 private:
  bool isfileExist();
  bool isRegularFile();
  bool hasNonZeroSize();

  std::string& m_filename;
  std::filesystem::path m_path;
};
}
