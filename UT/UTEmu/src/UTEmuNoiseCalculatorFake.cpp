/*
 * UTEmuNoiseCalculatorFake.cpp
 *
 *  Created on: Jan 3, 2015
 *      Author: ADendek
 */

#include "UTEmuNoiseCalculatorFake.h"

using namespace UTEmu;

NoiseCalculatorFake::NoiseCalculatorFake() {}

void NoiseCalculatorFake::updateNoise(RawData<double>* /*p_inputData*/) {}

void NoiseCalculatorFake::saveNoiseToFile(const std::string& /*p_filename*/) {}
