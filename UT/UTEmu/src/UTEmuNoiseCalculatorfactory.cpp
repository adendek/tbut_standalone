/*
 * UTEmuNoiseCalculatorfactory.cpp
 *
 *  Created on: Jan 3, 2015
 *      Author: ADendek
 */

#include "UTEmuNoiseCalculatorfactory.h"
#include "UTEmuNoiseCalculator.h"
#include "UTEmuNoiseCalculatorFake.h"

using namespace UTEmu;

NoiseCalculatorFactory::NoiseCalculatorFactory() {}

NoiseCalculatorFactory::NoiseCalcualtorPtr
NoiseCalculatorFactory::createNoiseCalculator(
    const std::string& p_noiseCalculatorType) {
  if (p_noiseCalculatorType == UTEmu::NoiseCalculatorType::calculator)
    return NoiseCalcualtorPtr(new NoiseCalculator());
  else if (p_noiseCalculatorType == UTEmu::NoiseCalculatorType::fake)
    return NoiseCalcualtorPtr(new NoiseCalculatorFake());

  else
    throw NoSuchState(p_noiseCalculatorType);
}
