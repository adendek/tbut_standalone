/*
 * UTEmuPedestalRetreiver.h
 *
 *  Created on: Jan 2, 2015
 *      Author: ADendek
 */

#pragma once

#include "UTEmuIPedestalFollowing.h"
#include "UTEmuPedestal.h"
#include "UTEmuIFileValidator.h"

namespace UTEmu {

class PedestalRetreiver : public IPedestalFollowing {
 public:
  PedestalRetreiver(Pedestal& p_pedestal, IFileValidator& p_fileValidator,
                    const std::string& p_filename);

  StatusCode processEvent(RawData<>* p_data) override;
  void savePedestalToFile(const std::string& p_filename) override;

 private:
  void getPedestalFromFile();

  bool m_isFillingPedestalRequited;
  Pedestal& m_pedestal;
  IFileValidator& m_fileValidator;
  const std::string& m_filename;
};

} /* namespace UTEmu */
