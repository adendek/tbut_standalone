/*
 * UTEmuMamdaDataReader.h
 *
 *  Created on: Jun 24, 2015
 *      Author: ADendek
 */
#pragma once

#include "UTEmuIDataReader.h"
#include "UTEmuIFileValidator.h"
#include "UTEmu/UTEmuRawData.h"
#include <string>
#include <fstream>
#include <vector>
#include <map>
#include <stdio.h>
#include <memory>


namespace UTEmu {

class TxtDataReader : public IDataReader {
 public:
  TxtDataReader(std::string& p_fileName, IFileValidator& p_fileValidator);

  void checkInput() override;
  RawData<>* getEventData() override;

 private:

  std::vector<int> getDataFromBinFile(unsigned char* array, int size);
  int get_rs_mask(int shift);
  void shift_right(unsigned char* buf, int msg_len, int shift);
  int getNBits(unsigned char* array,int size,  int nbits);
  int get6bitNumber(unsigned char*  container,int size);

  IFileValidator& m_fileValidator;
  std::string& m_fileName;
  FILE* inputFile;
};

} /* namespace UTEmu */
