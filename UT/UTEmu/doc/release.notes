!-----------------------------------------------------------------------------
! Package     : Tb/UTEmu
! Responsible : Adam Dendek, Jinlin Fu
! Purpose     : Algorithms for UT testbeam analysis
!-----------------------------------------------------------------------------

!============================ UTEmu v1r2 2017-02-23 ===========================

! 2016-05-13 - Steve Blusk
 - Updates of ClusterWithTrackAna package.

! 2015-11-06 - Adam Dendek
 - Fix duplicated cluster issue.
 - Improve distinction between different sensor types.

! 2015-10-27 - Steve Blusk
 - Overhaul ClusterWithTrackAna to use more flexible approach.
   Also includes CMS data

! 2015-10-15 - P. Manning
 - Updating AnalysisBase.

! 2015-10-15 - Adam Dendek
 - Fix bug in cluster creator algorithm.

! 2015-10-13 - P. Manning
 - Adding track matching and noise script.
 - Adding first version of a stripped down analysis for easy editing.

! 2015-10-08 - Steve Blusk
 - Updates to ClusterAnaWithTrack.

! 2015-09-09 - Adam Dendek
 - Add ability to read data collected by D-type sensor.
 - Fix bug in noise calculator.
 - Add TDC information to the nTuple.

! 2015-08-28 - Adam Dendek
 - Add cluster threshold as a tunable parameter and remove ZS algorithm.
 - Add cluster number per event monitoring histogram.

! 2015-07-14 - Adam Dendek
 - Add Mamba board data decoder.

!============================ UTEmu v1r1 2015-05-19 ===========================

! 2015-03-31 - Adam Dendek
 - Many new classes.

!============================ UTEmu v1r0 2014-11-30 ===========================

! 2014-11-17 - Heinrich Schindler
 - Initial import (copy from Adam's git repository)
 - Fix some trivial compiler warnings
