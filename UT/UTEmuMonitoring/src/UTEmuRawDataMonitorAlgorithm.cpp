/*
 * UTEmuRawDataMonitorAlgorithm.cpp
 *
 *  Created on: Oct 7, 2014
 *      Author: ADendek
 */
#include "UTEmuRawDataMonitorAlgorithm.h"
#include "UTEmu/UTEmuDataLocations.h"

DECLARE_COMPONENT( UTEmu::RawDataMonitorAlgorithm )

using namespace UTEmu;

RawDataMonitorAlgorithm::RawDataMonitorAlgorithm(const std::string& name,
                                                 ISvcLocator* pSvcLocator)
    : DataMonitorAlgorithm(name, pSvcLocator) {
  RawDataMonitorAlgorithm::m_inputDataLoc = UTEmu::DataLocations::RawTES;
}

StatusCode RawDataMonitorAlgorithm::finalize() {
  DataMonitorAlgorithm::m_outpuProjectionHistogramName = "ProjectionRawData";
  return DataMonitorAlgorithm::finalize();
}
