/*
 * UTEmuCommonModeSubtractorDataMonitorAlgorithm.h
 *
 *  Created on: Nov 26, 2014
 *      Author: ADendek
 */

#pragma once

#include "UTEmuDataMonitorAlgorithm.h"
#include "UTEmu/UTEmuNoise.h"
#include <map>

namespace UTEmu {

class CommonModeSubtractorDataMonitorAlgorithm : public DataMonitorAlgorithm {
 public:
  CommonModeSubtractorDataMonitorAlgorithm(const std::string& name,
                                           ISvcLocator* pSvcLocator);
  StatusCode execute() override;
  StatusCode finalize() override;

 private:
  typedef std::map<int, TH1D*> HistogramMap;

  StatusCode getData() override;

  StatusCode saveSimpleEvents() override;
  virtual StatusCode fillOnly2DHistogram() override;

  void createHistogram2D() override;
  TH2D* bookHistogram2D(const std::string& p_histogramName,
                        const std::string& p_histogramTitle,
                        int p_sensorNumber) override;

  std::string createHistogramTitle() override;
  std::string createHistogramName() override;

  void fillHistogram2D() override;
  void fillHistogram(TH1D* p_histogram) override;

  void createNoiseHistograms();
  void fillNoiseHistograms();

  UTEmu::RawDataContainer<double>* m_dataContainer;
  UTEmu::RawData<double> m_data;

  Noise m_noise;

  HistogramMap m_noisePerChannelHistograms;
  HistogramMap m_noisePerSensorHistograms;
};
}
