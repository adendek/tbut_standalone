/*
 * UTEmuPedestalDataMonitorAlgorithm.cpp
 *
 *  Created on: Oct 18, 2014
 *      Author: ADendek
 */


#include "UTEmuPedestalSubtractorDataMonitorAlgorithm.h"
#include "UTEmu/UTEmuDataLocations.h"
#include "UTEmu/UTEmuRawData.h"
#include "GaudiUtils/Aida2ROOT.h"

DECLARE_COMPONENT( UTEmu::PedestalSubtractorDataMonitorAlgorithm )

using namespace UTEmu;

PedestalSubtractorDataMonitorAlgorithm::PedestalSubtractorDataMonitorAlgorithm(
    const std::string& name, ISvcLocator* pSvcLocator)
    : DataMonitorAlgorithm(name, pSvcLocator) {
  DataMonitorAlgorithm::m_inputDataLoc = UTEmu::DataLocations::PedestalTES;
}

std::string PedestalSubtractorDataMonitorAlgorithm::createHistogramName() {
  return "Data_after_pedestal_event_" +  std::to_string(m_evtNumber);
}

TH2D* PedestalSubtractorDataMonitorAlgorithm::bookHistogram2D(
    const std::string& p_histogramName, const std::string& p_histogramTitle,
    int p_sensorNumber) {
  int l_ylow = -40;
  int l_yhigh = 40;
  int l_ybin = 1600;
  return Gaudi::Utils::Aida2ROOT::aida2root(book2D(
      p_histogramName, p_histogramTitle, -0.5 + RawData<>::getMinChannel(),
      RawData<>::getMaxChannel() + 0.5, p_sensorNumber, l_ylow, l_yhigh,
      l_ybin));
}

std::string PedestalSubtractorDataMonitorAlgorithm::createHistogramTitle() {
  return "Data after Pedestal Subtraction - event" +  std::to_string(m_evtNumber);
  }

void PedestalSubtractorDataMonitorAlgorithm::createHistogram2D() {
  std::string l_histogramName = "PedestalData_vs_channel";
  std::string l_histogramTtttle = "Data after Pedestal vs channel";
  int l_sensorNum = RawData<>::getnChannelNumber();
  m_histogram2D =
      bookHistogram2D(l_histogramName, l_histogramTtttle, l_sensorNum);
}

StatusCode PedestalSubtractorDataMonitorAlgorithm::finalize() {
  DataMonitorAlgorithm::m_outpuProjectionHistogramName = "ProjectionPedestal";
  return DataMonitorAlgorithm::finalize();
}
