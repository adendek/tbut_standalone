/*
 * UTEmuPedestalDataMonitorAlgorithm.h
 *
 *  Created on: Oct 18, 2014
 *      Author: ADendek
 */
#pragma once

#include "UTEmuDataMonitorAlgorithm.h"

namespace UTEmu {

class PedestalSubtractorDataMonitorAlgorithm : public DataMonitorAlgorithm {
 public:
  PedestalSubtractorDataMonitorAlgorithm(const std::string& name,
                                         ISvcLocator* pSvcLocator);
  StatusCode finalize() override;

 private:
  TH2D* bookHistogram2D(const std::string& p_histogramName,
                        const std::string& p_histogramTitle,
                        int p_sensorNumber) override;
  std::string createHistogramTitle() override;
  void createHistogram2D() override;
  std::string createHistogramName() override;
};
}
