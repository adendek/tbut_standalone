import random
import numpy as np

def generate_frame():
    n_entries = 1000
    frame_len = 128
    adc_length_in_bits = 6
    bites_per_byte = 8
    frame_len_in_bytes = int((adc_length_in_bits+1)*frame_len)/bites_per_byte

    with open("frame.txt","wb") as outfile:
        for entry in range(n_entries):
            random_frame = np.random.bytes(frame_len_in_bytes)
            outfile.write(random_frame)

if __name__ == "__main__":
    generate_frame()
