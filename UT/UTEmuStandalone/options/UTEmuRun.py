__author__ = 'A. Dendek'

from Gaudi.Configuration import *
from Configurables import UTEmuStandalone

UTEmuStandalone().InputFile = '$UTEMUSTANDALONEROOT/options/UT/frame.txt'
UTEmuStandalone().isPedestal = False
UTEmuStandalone().isAType=True
UTEmuStandalone().Monitoring=True


UTEmuStandalone().EvtMax=-1
UTEmuStandalone().PedestalInputData ="$UTEMUSTANDALONEROOT/options/UT/ped.txt"



