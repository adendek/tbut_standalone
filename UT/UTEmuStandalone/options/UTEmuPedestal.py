__author__ = 'ja'

from Gaudi.Configuration import *
from Configurables import UTEmuStandalone


UTEmuStandalone().InputFile = '$UTEMUSTANDALONEROOT/options/UT/frame.txt'
UTEmuStandalone().isAType=False

UTEmuStandalone().EvtMax=-1
UTEmuStandalone().PedestalOutputData ="$UTEMUSTANDALONEROOT/options/UT/ped.txt"
