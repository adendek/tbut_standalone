
"""
High level configuration tools for UTEmu standalone
This  file is based on Kepler configuration, see https://gitlab.cern.ch/lhcb/Kepler/blob/master/Tb/Kepler/python/Kepler/Configuration.py
"""
__version__ = "1.0"
__author__  = "A. Dendek"

from Gaudi.Configuration import *
import GaudiKernel.ProcessJobOptions
from Configurables import LHCbConfigurableUser, LHCbApp


from Configurables import UTEmu__RawDataReaderAlgorithm as rawDataReader
from Configurables import UTEmu__RawDataMonitorAlgorithm as rawDataMonitor
from Configurables import UTEmu__PedestalSubtractorAlgorithm  as pedestalSubtractor
from Configurables import UTEmu__PedestalSubtractorDataMonitorAlgorithm as pedestalMonitor
from Configurables import UTEmu__CommonModeSubtractorAlgorithm  as CMS
from Configurables import UTEmu__CommonModeSubtractorDataMonitorAlgorithm  as CMSMonitor
from Configurables import UTEmu__ClusterCreatorAlgorithm as ClusterCreator
from Configurables import UTEmu__ClusterCreatorDataMonitorAlgorithm as ClusterCreatorMonitor
from Configurables import UTEmu__NTupleCreator as nTupleCreator


class UTEmuStandalone(LHCbConfigurableUser):

    __slots__ = {
        # Input
        "InputFile"         :   "/afs/cern.ch/user/a/adendek/new_UTEmu/UTEmu_standalone/UT/UTEmuStandalone/options/UT/frame.txt",            # Names of input file 
        "EvtMax"             :   -1,            # Max. number of events to process
        "isPedestal"         :   True,
        "isAType"             :   True,          # Flag to
        # Output
        "HistogramFile"      :   "",            # Name of output histogram file
        "PedestalOutputData" :   "",            #
        "PedestalInputData" :   "",            #
        "TupleFile"          :   "",            # Name of output tuple file
        "WriteTuples"        :   False,         # Flag to write out tuple file or not
        # Options
        "Monitoring"         :   False,          # Flag to run the monitoring sequence
        "UserAlgorithms"     :   [],             # List of user algorithms not typically run
	    "EventsToDisplay"    :  100             # number of events to save into output file 
    }

    _propertyDocDct = {
        'InputFile'      : """ Name of input raw data file """,
        'isPedestal'      : """ Flag to run the pedestal calculation sequence """,
        "isAType"         : """ Flag to sepcify whether the sensor is A type""",
        'EvtMax'          : """ Maximum number of events to process """,
        'HistogramFile'   : """ Name of output histogram file """,
        'PedestalOutputData': """ Name of output pedestal file """,
	'PedestalInputData': """ location of the pre calcualted pedestal sums """,
        'TupleFile'       : """ Name of output tuple file """,
        'WriteTuples'     : """ Flag to write out tuple files or not """,
        'Monitoring'      : """ Flag to run the monitoring sequence or not """,
        'UserAlgorithms'  : """ List of user algorithms """
    }

    __used_configurables__ = [LHCbApp,LHCbConfigurableUser]


    def configureSequence(self):
        """
        Set up the top level sequence and its phases
        """
        log.info("Configure sequence: start")
        UTEmu_seq = GaudiSequencer("UTEmuStandaloneSequencer")
        ApplicationMgr().TopAlg = [UTEmu_seq]
        if self.getProp("isPedestal") == True:
            log.info("Configure sequence: isPedestal")
            pedestal_seq = GaudiSequencer("Pedestal")
            self.configurePedestal(pedestal_seq)
            UTEmu_seq.Members += [pedestal_seq]
        else:
            log.info("Configure sequence: isRun")
            run_seq = GaudiSequencer("Run")
            self.configureRun(run_seq)
            UTEmu_seq.Members += [run_seq]
            if self.getProp("Monitoring") == True:
                log.info("Configure sequence: monitoring enabled")
                monitoringSeq = GaudiSequencer("Monitoring")
                self.configureMonitoring(monitoringSeq)
                UTEmu_seq.Members += [monitoringSeq]

        UserAlgorithms = self.getProp("UserAlgorithms")
        if (len(UserAlgorithms) != 0):
            userSeq = GaudiSequencer("UserSequence")
            userSeq.Members = UserAlgorithms
            UTEmu_seq.Members += [userSeq]
        log.info("Configure sequence: end")


    def configureInput(self):
        log.info("Configure input: start")
        # No events are read as input
        ApplicationMgr().EvtSel = 'NONE'
        # Delegate handling of max. number of events to ApplicationMgr
        self.setOtherProps(ApplicationMgr(), ["EvtMax"])
        # Transient store setup
        EventDataSvc().ForceLeaves = True
        EventDataSvc().RootCLID    =    1
        log.info("Configure input: end")


    def configureOutput(self):
        log.info("Configure output:  start")

        # ROOT persistency for histograms
        ApplicationMgr().HistogramPersistency = "ROOT"
        # Set histogram file name.
        histoFile = "UTEmu-histos.root"
        if (self.isPropertySet("HistogramFile") and self.getProp("HistogramFile") != ""):
            histoFile = self.getProp("HistogramFile")
        HistogramPersistencySvc().OutputFile = histoFile
        # Set tuple file name.
        tupleFile = "UTEmu-tuple.root"
        if (self.isPropertySet('TupleFile') and self.getProp("TupleFile") != ""):
            tupleFile = self.getProp("TupleFile")
        ApplicationMgr().ExtSvc += [NTupleSvc()]
        tupleStr = "FILE1 DATAFILE='%s' TYP='ROOT' OPT='NEW'" % tupleFile
        NTupleSvc().Output += [tupleStr]
        from Configurables import MessageSvc
        MessageSvc().setWarning += ['RFileCnv', 'RCWNTupleCnv']
        log.info("Configure output:  start")



    def configurePedestal(self, seq):
        rawDataReader().isAType=self.getProp("isAType")
        rawDataReader().inputData= self.getProp("InputFile")
    
        pedestalSubtractor().ChannelMaskInputLocation= "$UTEMUSTANDALONEROOT/options/UT/MambaMasks.dat"
        pedestalSubtractor().PedestalOutputFile=self.getProp("PedestalOutputData")
        pedestalSubtractor().treningEntry=400

        seq.Members =[rawDataReader(),pedestalSubtractor()]


    def configureRun(self, seq):
        rawDataReader().inputData=self.getProp("InputFile")
        rawDataReader().isAType=self.getProp("isAType")
        pedestalSubtractor().ChannelMaskInputLocation= "$UTEMUSTANDALONEROOT/options/UT/MambaMasks.dat"
        pedestalSubtractor().PedestalInputFile=self.getProp("PedestalInputData")
        pedestalSubtractor().FollowingOption='file'
        pedestalSubtractor().treningEntry=1
        CMS().ChannelMaskInputLocation = "$UTEMUSTANDALONEROOT/options/UT/MambaMasks.dat"
        CMS().NoiseOutputFile = "$UTEMUSTANDALONEROOT/options/UT/noise_Mamba.dat"
        ClusterCreator().NoiseInputFile = "$UTEMUSTANDALONEROOT/options/UT/noise_Mamba.dat"
        ClusterCreator().LowThreshold = 2.5
        ClusterCreator().HighThreshold = 3
        if self.getProp("isAType"):
            sensor_type = "AType"
        else: 
            sensor_type = "PType"
        ClusterCreator().sensorType = sensor_type
        nTupleCreator().StoreEventNumber=900000
        nTupleCreator().WriteRaw = False
        nTupleCreator().WriteHeader = True
        nTupleCreator().WritePedestal = False
        nTupleCreator().WriteCMS = False
        seq.Members = [rawDataReader() ,pedestalSubtractor(), CMS(), ClusterCreator(), nTupleCreator()]

    def configureMonitoring(self, seq):
        rawDataMonitor().displayEventNumber=self.getProp("EventsToDisplay")
        pedestalMonitor().displayEventNumber=self.getProp("EventsToDisplay")
        CMSMonitor().displayEventNumber=self.getProp("EventsToDisplay")
        ClusterCreatorMonitor().displayEventNumber=self.getProp("EventsToDisplay")
        if self.getProp("isAType"):
            sensor_type = "AType"
        else: 
            sensor_type = "PType"
        ClusterCreatorMonitor().sensorType = sensor_type
        seq.Members = [rawDataMonitor() ,pedestalMonitor(), CMSMonitor(),ClusterCreatorMonitor()]


    def evtMax(self):
        return LHCbApp().evtMax()

    def __apply_configuration__(self):
        GaudiKernel.ProcessJobOptions.PrintOn()
        self.configureInput()
        self.configureSequence()
        self.configureOutput()
        GaudiKernel.ProcessJobOptions.PrintOn()
        log.info(self)
        GaudiKernel.ProcessJobOptions.PrintOff()


