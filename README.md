# UTEmu Standalone
The goal of this project is to detach UTEmu from Kepler and keep it as a separate Gaudi based project. 
This page will be updated soon. 

## Getting Started

These instructions will get you a copy of the project up and running on lxplus for development and testing purposes. 


### Installing

This section contains instruction on how to install the software. 
First, you need to clone the repository 

```
git clone https://gitlab.cern.ch/adendek/UTEmu_standalone.git 
```

Then, in order to build the software you need to run the following commands:  

```
cd UTEmu_standalone
lb-project-init
make -j10
```

The dummy data can be genetated via frameGenerator script:

```
cd UT/UTEmuStandalone/scripts
python frameGenerator.py
```



## Running the project

To run the project you can use the following options: 

``` bash
# for pedestal
./build.x86_64-centos7-gcc8-opt/run gaudirun.py UT/UTEmuStandalone/options/UTEmuPedestal.py 
# for run
./build.x86_64-centos7-gcc8-opt/run gaudirun.py UT/UTEmuStandalone/options/UTEmuRun.py 

```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Adam Dendek** - *Initial work* - [adam](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

